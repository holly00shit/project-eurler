#!/usr/bin/env python
# -*- coding: utf-8 -*-

__range_wide = xrange(2, 101)

rels = []

for a in __range_wide:
    for b in __range_wide:
        term = a ** b
        if term not in rels:
            rels.append(term)

print rels.__len__()