#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import time

def primeSieve(n):
    primes = [True] * n
    for i in range(2, n**1/2):
        for k in range(i*2, n+1, i):
            primes[k - 1] = False
    return [ i+1 for i in range(len(primes)) if primes[i]]

def isprime(n):
    if n < 0: return False           # notice this.
    for i in range(2, (n**1/2)+1):
        if n%i == 0:
            return False
    return True

start = time.time()
primes = primeSieve(1000)
max_pair = (0,0,0)
for b in primes:
    for a in xrange(1-b, 1000):
    #for b in primes: # b >= 2, a + b + 1 >= 2
        n, count = 0, 0
        while True:
            v = n*n + a*n + b
            if v in primes: count = count + 1
            else: break
            n = n + 1
        if count > max_pair[2]:
            max_pair = (a,b,count)

print max_pair[0]*max_pair[1]
print 'Elapsed Time = %f seconds'% (time.time() - start)

"""

def prime_sieve(n):
    primes = [True] * n
    for i in range(2, n**1/2):
        for k in range(i*2, n+1, i):
            primes[k - 1] = False
    return [ i+1 for i in range(len(primes)) if primes[i]]

def is_prime(n):
    if n < 0: return False           # notice this.
    for i in range(2, (n**1/2)+1):
        if n%i == 0:
            return False
    return True

primes = prime_sieve(1000)
result = 0
max_sequence = 0

for a in range(-999,1000):
    for b in primes:
        n = 0
        fn_result = n*(n+a)+b
        while (fn_result < 1000 and (fn_result in primes)) or is_prime(fn_result):
            n += 1
            fn_result = n*(n+a)+b
        if n > max_sequence:
            result = a*b
            max_sequence = n
            print ('Found a sequence of length {}. (a,b)=({},{})'.format(n,a,b))

print ('Solution is {}'.format(result))
"""