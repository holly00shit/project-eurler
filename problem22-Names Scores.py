#!/usr/bin/env python
# -*- coding: utf-8 -*- 

with open('names.txt', 'r') as fp:
    content = fp.read()
    names = content.replace('\"', '').split(',')
    names =  sorted(names)
    sums = 0
    for name in names:
        sums += (names.index(name)+1) * sum(map(lambda x: ord(x)-64, name))
    print sums