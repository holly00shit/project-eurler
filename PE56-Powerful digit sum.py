#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
A googol (10100) is a massive number: one followed by one-hundred zeros;
100100 is almost unimaginably large: one followed by two-hundred zeros.
 Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?
'''


largest = 0
largest_t = 0
for a in range(1, 100):
    for b in range(1, 100):
        d = pow(a, b)
        digits_sum = sum(int(i) for i in list(str(d)))
        if digits_sum > largest:
            largest_t = (a, b, d)
            largest = digits_sum

print largest, largest_t