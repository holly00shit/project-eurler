#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
'''

Flag = True
num = 10000
while Flag:
    num = num + 1
    std = sorted(str(2 * num))
    stdlen = len(str(2 * num))
    m = True
    for i in range(3, 7):
        if sorted(str(i * num)) != std or len(str(i * num)) != stdlen:
            m = False
            break
    if not m: continue
    else: Flag = False

    if num > 1000000: Flag = False

print num, '###'