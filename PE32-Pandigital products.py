#!/usr/bin/env python
# -*- coding: utf-8 -*- 

# 39 × 186 = 7254

'''
def get_distinct_item(items, n):
    if n == 0:
        yield []
        return
    for i, item in enumerate(items):
        this_one = [ item ]
        items_ = items[:i] + items[i+1:]
        for cc in get_distinct_item(items_, n-1):
            yield this_one + cc

def num(n):
    s = 0
    for l in n: s = s*10 + l
    return s

product = {}
for perm in get_distinct_item(range(1, 10), 9):
    for cross in range(1,4):
        for eq in range(cross+1, 6):
            a = num(perm[0:cross])
            b = num(perm[cross:eq])
            c = num(perm[eq:9])
            if a * b == c: product[c] = 1

print sum(p for p in product)
'''

p = set()
for a in range(1, 100):
    for b in range(1, 9999 // a):
        if ''.join(sorted("%d%d%d" % (a, b, a*b))) == '123456789':
            p.add(a * b)
print(sum(p))