#!/usr/bin/env python
# -*- coding: utf-8 -*-
#求最大的时候考虑从上往下走， 不得已再去brute force

MAX = 1000000
prime = [True]*MAX
primeList = []
def prime_sieve():
    global  prime
    for i in range(2, MAX**1/2 + 1):
        for n in range(i+i, MAX, i):
            prime[n] = False
        if prime[i]:
            primeList.append(i)
prime_sieve()


result = []
m = 0
h = 0
t = 0
for i in xrange(1, 5000):
    s = 0
    for k, p in enumerate(primeList[i:]):
        s += p
        if s >= 1000000: continue
        if prime[s]:
            maxlen = k
            maxPrime= s
    if maxlen > m:
        m = maxlen
        h = maxPrime
        t = primeList[i]


print h, m, t


#
#Fast
#for i in xrange(1000, 0, -1):
#    maxprime = 0
#    for x in xrange(0, 1000000):
#        total = sum(primeList[x:x+i])
#        if total > 1000000: break
#        if prime[total]: maxprime = total; print i, primeList[x:x+i]
#    if maxprime != 0:
#        print maxprime
#        break











