#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
'''

MAX = 100000
prime = [True]*MAX
def prime_sieve():
    global  prime
    for i in range(2, MAX**1/2 + 1):
        for n in range(i+i, MAX, i):
            prime[n] = False
prime_sieve()

def genFromList(srcList, midList):
    if midList.count('|') == 4:   #注意条件不是=, 而是>=, i有两位数，三位数...
        yield midList
    else:
        #for i in srcList:
        #    for j in genFromList(filter(lambda x:x!=i, srcList), midList+i+'|'): #有重复数字的会有问题如2969
        #        yield j
        for i, item in enumerate(srcList):
            for j in genFromList(srcList[:i] + srcList[i+1:], midList+item+'|'):  #good
                yield  j

done = []
for start in range(1000, 9999):
    if start in done: continue
    if not prime[start]: continue
    src = [i for i in str(start)]
    temp = []
    for k in genFromList(src, ''):
        if k.startswith('0'): continue
        next = int(''.join(k.split('|')))
        if not prime[next]: continue
        if next not in temp:
            temp.append(next)
    done.extend(temp)
    if temp.__len__() < 3: continue
    stemp = sorted(set(temp))
    for i in range(0, stemp.__len__()-1):
        for k in range(i+1, stemp.__len__()-1):
            if (stemp[k] - stemp[i] + stemp[k]) in stemp:
                print stemp[i], stemp[k], stemp[k] - stemp[i] +stemp[k]

