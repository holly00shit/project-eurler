#coding:utf8

from time import time

__author__ = 'Administrator'

def odd(n):
    return 3*n + 1

def even(n):
    return n/2

def FindlongestSqo(n):
    #初始化列表
    StepsofDot = {}
    for i in range(1,100):
        Dot = 2**i
        if Dot < n:
            StepsofDot[Dot] = i
#            print i,Dot
        else:
            break

    #遍历开始  个人感觉要提高效率的话只有把每次遍历的路径都记录下来，以避免重复遍历，但是需要反复的比较判断，较为繁琐
    step_detai = {}
    for i in range(2,n):
        tmp = i
        step = 0
        while not StepsofDot.has_key(tmp):
            step += 1
            if tmp%2 == 0:
                tmp = even(tmp)
            else:
                tmp = odd(tmp)
        StepsofDot[i] = step + StepsofDot[tmp] #记录遍历过的点

    ans = []
    p = 0
    for i in StepsofDot:
        if StepsofDot[i] > p:
            p = StepsofDot[i]
            ans = [i,p]
    print ans


if __name__ == '__main__':
    start_time = time()
    FindlongestSqo(1000000)
    end_time = time()
    print 'process time : %f' %(end_time - start_time)
