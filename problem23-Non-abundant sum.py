#!/usr/bin/env python
# -*- coding: utf-8 -*-
#brute force

import math, time

def isAbundantNum(num):
    divisors = []
    for i in range(1, int(math.sqrt(num)+1)):
        if num%i == 0:
            divisors.append(i)
            if num/i not in divisors and num/i != num:
                divisors.append(num/i)
    return sum(divisors) > num

start = time.time()
abundants = []
flag = {}
wewant = range(1, 28124)
for num in range(1, 28124):
    flag [num] = 1
    if isAbundantNum(num):
        abundants.append(num)

for j in xrange(abundants.__len__()):
    for k in xrange(j, abundants.__len__()):
        tmp = abundants[j] + abundants[k]
        if tmp < 28124:
            flag[tmp] = 0

print sum([i for i in wewant if flag[i]])

print time.time() - start
#4179871, 7.84299993515s

#optimize
'''
t = time()

def problem23():

    def d(n):

        m, s = n ** 0.5, 1
        if m == int(m): s -= int(m)
        m = int(m//1) + 1
        for i in xrange(2, m):
            if n%i == 0: s += i + (n/i)
        return s

    a, s = set(), 0
    for i in xrange(1, 20612):
        if d(i) > i: a.add(i)
        if not any((i - j) in a for j in a): s += i
    return s
print problem23()
print time() - t
'''