#coding: UTF-8
__author__ = 'Administrator'

def Multiple():
    end = 1
    for i in xrange(2,21):
        end = LCM(end,i)
        print end,'*',i
    print end

def GCD(x,y):
    """GCD容易一些 因为最大公约数是交集是and操作 只有这一种情况"""
    large = x
    small = y
    if large < small:
        large,small = small,large
    if small == 0:
        return 0
    a =  fi(large,small)
    return a

def fi(x,y):
    tmp = 0
    a = x
    b = y
    if b <> 0:
        tmp = a % b
        if tmp == 0:
            return b
        else:
            return fi(b,tmp)

def LCM(x,y):
    if GCD(x,y) <> 0:
        return x*y/GCD(x,y)

if __name__ == '__main__':
    print GCD(45,20)
    Multiple()
