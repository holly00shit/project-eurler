#!/usr/bin/env python
# -*- coding: utf-8 -*- 

'''
The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
(Please note that the palindromic number, in either base, may not include leading zeros.)

http://projecteuler.net/index.php?section=problems&id=36
'''

def ispalindromic(num):
    return num == num[::-1]

sum = 0
for i in range(0, 1000000):
    if ispalindromic(str(i)) and ispalindromic(bin(i)[2:]):
        sum = sum + i
print sum