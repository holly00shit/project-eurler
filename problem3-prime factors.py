__author__ = 'Administrator'
import math
TargetNum = 600851475143

#质因数分解的好方法。
def PrimeFact(a):
    start = 2
    n = a
    c_flag = 1
    while c_flag:
        c_flag = 0
        for i in xrange(start,int(math.sqrt(n)+1)):
            if not n%i:
                n = n/i              #这一步很关键
                yield i
                start = i
                c_flag = 1
                break
    if n <> a:
        yield n

if __name__ == '__main__':
    print max(PrimeFact(TargetNum))
    print list(PrimeFact(TargetNum))

