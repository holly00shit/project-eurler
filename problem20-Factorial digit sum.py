#coding:utf8
__author__ = 'Administrator'

def Factorial(n):
    if n == 1:
        return 1
    return n*Factorial(n-1)


if __name__ == '__main__':
    ans = Factorial(100)
    print sum(int(i) for i in str(ans))

    # one_line code !!
    print sum(int(i) for i in str(reduce(lambda x,y:x*y , range(1,101))))

