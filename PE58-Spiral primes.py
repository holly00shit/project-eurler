#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.

37 36 35 34 33 32 31
38 17 16 15 14 13 30
39 18  5  4  3 12 29
40 19  6  1  2 11 28
41 20  7  8  9 10 27
42 21 22 23 24 25 26
43 44 45 46 47 48 49

It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.

If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?
'''
#
#MAX = 5000000
#prime = [True]*MAX
#def prime_sieve():
#    global  prime
#    for i in range(2, MAX**1/2 + 1):
#        for n in range(i+i, MAX, i):
#            prime[n] = False
#prime_sieve()
#

def is_prime_naive(n) :
    '''
    最快的判断是否是素数的函数...
    '''
    if n % 2 == 0 :
        return False

    for i in xrange(3, int(n**0.5 + 1), 2) :
        if n % i == 0 :
            return False
    return True

beg = 1
sidelen = 3
diagonals_prime = 0
total_diagonal = 1
while True:
    increment = sidelen - 1
    for i in range(0, 4):
        beg = beg + increment
        if is_prime_naive(beg):
            diagonals_prime = diagonals_prime + 1
    total_diagonal = total_diagonal + 4
    #print sidelen, diagonals_prime, total_diagonal
    if diagonals_prime * 10 < total_diagonal:
        print sidelen
        break
    sidelen = sidelen + 2





#start = 1
#add = 2
#
#number_count = 1
#prime_count = 0
#for i in xrange(100000) :
#    for j in xrange(4) :
#        start += add
#        number_count += 1
#        if is_prime_naive(start) :
#            prime_count += 1
#    add += 2
#    if float(prime_count) / number_count < 0.1 :
#        print add - 1
#        sys.exit()



