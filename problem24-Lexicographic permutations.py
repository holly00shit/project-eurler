#!/usr/bin/env python
# -*- coding: utf-8 -*- 


#circule
def c_factory(num):
    s = 1
    for u in range(1, num+1):
        s = s * u
    return s

def c_find(ms, n):
    perm = []
    while len(ms):
        d = c_factory(len(ms) - 1)
        pos = n / d
        n = n % d
        perm.append(ms[pos])
        ms = ms[0:pos] + ms[pos+1:]
    return perm

print ''.join(str(x) for x in c_find(range(0,10), 999999))


#recursive
def rec(ms, n, rel):
    if len(ms) == 0:
        return rel
    else:
        d = c_factory(len(ms) - 1)
        pos = n / d
        n = n % d
        rel += str(ms[pos])
        ms = ms[0:pos] + ms[pos+1:]
        return rec(ms, n, rel)

print rec(range(0, 10), 999999, '')