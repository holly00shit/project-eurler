#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
这个题目很烦人
'''

N = 8
MAX = 1000000
prime = [True]*MAX
primeList = []
def prime_sieve():
    global  prime
    for i in range(2, MAX**1/2 + 1):
        for n in range(i+i, MAX, i):
            prime[n] = False
        if prime[i]:
            primeList.append(i)
prime_sieve()

def hasSame(num):
    if num == 0: return 0, False
    num = int(str(num).replace('0', ''))
    first = num % 10
    for i in str(num):
        if i != str(first):
            return first, False
    return first, True

def hassameinlist(fl):
    if len(fl) == 1: return True
    first = fl[0]
    for i in fl:
        if i != first:
            return False
    return True

def get_same_ele(a, b):
    _ = [0]*len(a)
    p = [0]*len(a)
    ak = []
    bk = []
    for n, i in enumerate(a):
        if i == b[n]:
            _[n] = i
        elif i != b[n]:
            ak.append(i)
            bk.append(b[n])
            if b[n] > i:
                p[n] = str(int(b[n]) - int(i))
    if hassameinlist(ak) and hassameinlist(bk):
        return int(''.join(map(lambda x:str(x), _))), int(''.join(map(lambda x:str(x), p)))
    return int(''.join(map(lambda x:str(x), _))), -1

print get_same_ele(list(str(56003)), list(str(56113))), hasSame(110), int(str(110).replace(str(hasSame(110)[0]), '1'))

k = primeList[primeList.index(56003)-2: ]
for jprime in k:
    if jprime < 121312: continue #坑爹啊，不知道答案的话还不知道要跑多久。。
    for kprime in primeList[primeList.index(jprime)+1:]:
        if len(str(kprime)) > len(str(jprime)): continue
        common, minus = get_same_ele(list(str(jprime)), list(str(kprime)))
        if minus == -1:continue
        first, flag = hasSame(minus)
        if first >= 4: continue
        if flag:
            least = int(str(minus).replace(str(first), '1'))
            maxcount = 2
            tprime = kprime + least
            t = 0
            while len(str(tprime)) == len(str(jprime)):
                t = t + 1
                if prime[tprime]: maxcount = maxcount + 1
                tprime = tprime + least
                if get_same_ele(list(str(jprime)), list(str(tprime)))[-1] == -1: break
                if t >= 10: break
            if maxcount == 8:
                print jprime, kprime, maxcount, least
                raise Exception
























