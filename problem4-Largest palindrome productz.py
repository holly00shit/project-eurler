#coding:utf8
__author__ = 'Administrator'

def isPalindrome(n):
    return str(n) == ''.join(reversed(str(n)))

def FuncFind():
    result = 0
    for i in xrange(999,99,-1):
        if i*999 < result:         #less loop control
            break
        for j in xrange(999,i-1,-1):
            if i*j < result:        #less loop control
                break
            if isPalindrome(i*j) and i*j>result:
                result =  i*j
    return result

if __name__ == '__main__':
    print FuncFind()