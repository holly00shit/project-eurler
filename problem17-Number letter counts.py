# -*- coding: utf-8 -*-
"""
http://projecteuler.net/problem=17

If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are
3 + 3 + 5 + 4 + 4 = 19 letters used in total.
If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many
letters would be used?
NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23
letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out
numbers is in compliance with British usage.
"""

def addletters(i):
    ans = ''
    if i < 20:
        ans += lenofsigleWord[i]
    elif i < 100 and i >= 20:
        ans += (lenofsigleWord[i%10] + lenoftenWord[i//10])
    elif i < 1000 and i >= 100:
        if i%100 >= 20:
            ans += (lenofsigleWord[i//100] + 'hundred' + 'and' + lenoftenWord[i//10%10] + lenofsigleWord[i%10])
        elif i%100 ==0:                 #Notice !!!! at first time i ignore this case !!! so onehundredand apears!!!!
            ans += lenofsigleWord[i//100] + 'hundred'
        else:
            ans += (lenofsigleWord[i//100] + 'hundred' + 'and' + lenofsigleWord[i%100])
    elif i == 1000:
        ans += 'one' + 'thousand'
    return ans


if __name__ == '__main__':
    lenofsigleWord = {0:'',1:'one',2:'two',3:'three',4:'four',5:'five',6:'six',7:'seven',8:'eight',9:'nine',
                      10:'ten',11:'eleven',12:'twelve',13:'thirteen',14:'fourteen',15:'fifteen',16:'sixteen',
                      17:'seventeen',18:'eighteen',19:'nineteen'}
    lenoftenWord = {0:'',2:'twenty', 3:'thirty', 4:'forty', 5:'fifty', 6:'sixty', 7:'seventy',8: 'eighty', 9:'ninety'}
#    lenofhyphen = {'hundred':7,'thousand':8,'and':3}

    ans = ''
    for i in range(1,1001):
        ans += addletters(i)
    print len(ans)


