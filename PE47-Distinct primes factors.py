#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?
'''
#其实在用筛选法取素数的时候，就已经知道哪些数的约数是四个素数了。


#思路错误：
MAX = 100000
prime = [True]*MAX
def prime_sieve():
    global  prime
    for i in range(2, MAX**1/2 + 1):
        for n in range(i+i, MAX, i):
            prime[n] = False
prime_sieve()
primeList = filter(lambda x:prime[x], range(2, MAX))
##primeList = map(lambda x:str(x), primeList)
#
#def genFromList(srcList, num, midList):
#    if midList.count('|') == 4:   #注意条件不是=, 而是>=, i有两位数，三位数...
#        yield midList
#    else:
#        for i in srcList:
#            for j in genFromList(filter(lambda x:x!=i, srcList), num, midList+i+'|'):
#                yield j
#耗时太长..
def getDividers(num, ti):
    s = 2
    srcnum = num
    divids = []
    while True:
        if num % s == 0:
            num = num / s
            if s not in divids:
                divids.append(s)
            if s not in primeList:
                return False
            continue
        else:
            s = s + 1
        if s > srcnum**1/2 or num == 1:
            break
    if len(divids) != ti:
        return False
    return True


print getDividers(420, 4)
beg = 100000
while True:
    if not getDividers(beg, 4):
        beg += 1
        continue
    else: print beg
    if not getDividers(beg+1, 4):
        beg += 2
        continue
    if not getDividers(beg+2, 4):
        beg += 3
        continue
    if not getDividers(beg+3, 4):
        beg += 4
        continue
    print beg
    break



#very good.
#Limit=1000000     # Search under 1 million for now
#factors=[0]*Limit # number of prime factors.
#count=0
#for i in xrange(2,Limit):
#    if factors[i]==0:
#        # i is prime
#        count =0
#        val =i
#        while val < Limit:
#            factors[val] += 1
#            val+=i
#    elif factors[i] == 4:
#        count +=1
#        if count == 4:
#            print i-3 # First number
#            break
#    else:
#        count = 0
