#!/usr/bin/env python
# -*- coding: utf-8 -*- 

'''
如果一个数字将1到n的每个数字都使用且只使用了一次，我们将其称其为一个n位的pandigital数。
例如，2143是一个4位的pandigital数，并且是一个质数。

最大的n位pandigital质数是多少？

不可能是九位数和八位数
'''


def isPandigital(num, length = False):
    num_str = str(num)
    pans = []
    for i in num_str:
        i = int(i)
        if i not in range(1, 10):
            return False
        if i not in pans:
            pans.append(i)
        elif i in pans:
            return False
    if length and len(pans) != 9:
        return False
    return True

def isPrime(num):
    for i in xrange(2, num**1/2 + 1):
        if num%i == 0:
            return False
    return True


#for i in xrange(7654321, 1, -1):
#    if isPandigital(i) and isPrime(i):
#        print i
#        break

##============================================
def gen_pandigital(max_num):
    '''按照列表顺序全排列
    generates pandigital numbers starting with the biggest'''

    def generator(start, digits_left):
        if len(digits_left) == 1:
            yield start + digits_left[0]
        else:
            for i in digits_left:
                for p in generator(start + i, filter(lambda x: x != i, digits_left)):
                    yield p

    digits = [str(i) for i in range(max_num, 0, -1)]
    for i in generator('', digits):
        yield i


for n in gen_pandigital(7):
    if isPrime(int(n)):
        print n
        break










