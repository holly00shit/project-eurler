#coding:utf8
__author__ = 'Administrator'
import time

if __name__ == '__main__':
    StartTime = time.time()
    i2_pow_15 = 32768
    i2_pow_1000 = i2_pow_15**66 * 2**10
    result = sum(int(i) for i in str(i2_pow_1000) )

    print 'spend time :',time.time() - StartTime
    print result

    #OR

    print sum(map(int, str(2**1000)))
