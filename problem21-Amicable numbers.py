# -*- coding: utf-8 -*-
'''
http://projecteuler.net/problem=21

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a  b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
'''
import time

def FindAmcicable(n,limit):
    def sumofdivisors(d):
        sum = 0
        for i in range(1,d//2+1):
            if d%i ==0:
                sum += i
        return sum
    if n <= limit:
        A = sumofdivisors(n)
        if sumofdivisors(A) == n and A != n and A <= limit:
            return [n,A]
        else:
            return [n,0]
    else:  return [n,0]


if __name__ == '__main__':
    startTime = time.time()
    ans = []
    for i in range(3,10001):
        if i not in ans:
            L = FindAmcicable(i,10000)
            if L[1] != 0:
                ans += L

#    print FindAmcicable(220,1000)
    print ans,'**',sum(ans)
    print 'porcess time is :'.ljust(20),time.time() - startTime



