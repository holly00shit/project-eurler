#!/usr/bin/env python
# -*- coding: utf-8 -*- 

# 4*(2*n -1) + s - 4 or (n+1) ** 2

TT = 1001

s = 1
diag_total = 1
for n in xrange(2, (TT+1)/2 + 1):
    end = 4*(2*n -1) + s - 4 # or end = (n+1) ** 2
    minu = 2*(n - 1)
    diag_total += end + (end - minu) + (end - 2*minu) + (end - 3*minu)
    s = end

print diag_total




