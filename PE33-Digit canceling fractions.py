# -*- coding: utf-8 -*-

def have_same_digit(a, b):
    for i in str(a).strip('0'):
        if i in str(b):
            return True, (float(str(a).replace(i, '', 1)), float(str(b).replace(i, '', 1)))
    return False, ()

numerators = 1
denominators = 1
for denominator in range(11, 100):
    for numerator in range(10, denominator):
        _suit, fraction =  have_same_digit(numerator, denominator)
        if _suit and fraction[1] != 0.0 and\
            fraction[0]/fraction[1] == float(numerator)/denominator:
            # Since using floating point numbers is not a good idea
            # fraction[0] * denominator = fraction[1] * float(numerator)
            numerators = numerators * numerator
            denominators = denominators * denominator

a = denominators
while numerators > 0:
    denominators = denominators % numerators
    denominators, numerators = numerators, denominators

print a/denominators