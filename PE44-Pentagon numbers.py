#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
minDiff = 99999999
Pentagonals = []
for i in range(0, 3000):
    Pentagonals.append(i*(3*i-1)/2)

MAX = 2000
pent = [ n * (3*n - 1) / 2 for n in xrange(1, 2*MAX) ]
pdic = dict.fromkeys(pent)

for i in range(1, 3000):
    for k in range(i+1, 3000):
        pk = Pentagonals[k]
        pi = Pentagonals[i]
        padd = pk + pi
        pdiff = pk - pi
        if pdic.has_key(padd) and pdic.has_key(pdiff):
            print pdiff
            break


print minDiff
#说明：has_key比in操作要快很多。
#==================
#MAX = 2000
#pent = [ n * (3*n - 1) / 2 for n in xrange(1, 2*MAX) ]
#pdic = dict.fromkeys(pent)
#
#def main2():
#     for j in xrange(0, MAX):
#         for k in xrange(j+1, 2*MAX-1):
#             p_j = pent[j]
#             p_k = pent[k]
#             p_sum = p_j + p_k
#             p_diff = p_k - p_j
#             if pdic.has_key(p_sum) and pdic.has_key(p_diff):
#               return p_diff
#
#print main2()


#========================






#from itertools import *
#from math import *
#from operator import *
#
#def pentagonal(n):
#    return n*(3*n-1)//2
#
#def main():
#    pentagonals = set(pentagonal(n) for n in range(1, 3000))
#    c = combinations(pentagonals, 2)
#    for p in c:
#        if add(*p) in pentagonals and abs(sub(*p)) in pentagonals:
#            print p, list(pentagonals).index(7042750)
#            print(abs(sub(*p)))
#
#if __name__ == "__main__":
#    main()
