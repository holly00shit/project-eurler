#coding:utf8
#http://projecteuler.net/index.php?section=problems&id=12
#zhongks date:2013-01-11

from time import *
import math

__author__ = 'Administrator'


def factor_count(x):                  #brute force
    count = 0
    sx = int(math.sqrt(x))
    for i in range(1, sx+1):
        if x % i == 0:
            count += 2
    if sx * sx == x:
        count -= 1
    return count

def TriangleList():
    #初始化一个质数列表
    m = range(0,2000)
    for x in range(2,int(2000**1/2)):
        for i in range(2*x,2000,x):
            m[i] = 0

    prime = []
    for i in m:
        if i != 0 and i != 1:
            prime.append(i)
#    print prime

    #构造一个三角数的list
    triangle = []
    for i in range(1,50000):
        triangle.append((i+1)*i/2)

   #遍历三角数list开始
    for i in  triangle:
        ans = i
        li = []
        for j in prime:
            if i >= j:                  #之前是少了'=' 导致结果不正确
                if i%j != 0:
                    continue
                count = 0
                while i%j == 0:
                    i /= j
                    count += 1
                li.append(count)
            else: break
        s = 1
        for p in li:
            s *= (p+1)
        if s > 500:
            print ans
            break

# 76576500
if __name__ == '__main__':
    startTime = time()
    TriangleList()
    print 'Process Time is %f' %(time() - startTime)




