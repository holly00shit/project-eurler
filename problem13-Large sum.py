#coding:utf8
__author__ = 'Administrator'

def dataSum(linedata):
    return reduce(lambda x,y:x+y ,[linedata[i] for i in range(0,10)])

if __name__ == '__main__':
    fp = open('one-hundred-50-digit.txt','r')
    datalist = []
    sum = 0
    for linenum in range(0,100):
        sum += int(fp.readline())
    print str(sum)[:10]

    fp.close()

