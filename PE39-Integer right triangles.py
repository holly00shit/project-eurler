#!/usr/bin/env python
# -*- coding: utf-8 -*- 

'''
如果p是一个直角三角形的周长，三角形的三边长{a,b,c}都是整数。对于p = 120一共有三组解：

{20,48,52}, {24,45,51}, {30,40,50}

对于1000以下的p中，哪一个能够产生最多的解？
'''
import math
cc = {}
#for p in range(1000):
#    cc[p] = 0

for i in range(1, 500):
    for k in range(1, 500):
        if k < i: continue
        t = math.sqrt(i**2 + k**2)
        q = t+i+k
        if int((q-i-k))**2 == i**2 + k**2 and q < 1000:
            cc[int(q)] = cc.get(q, 0) + 1
#rel = 0
#key = 0
#for c in cc:
#    if cc[c] > rel:
#        rel = cc[c]
#        key = c
#print rel, key

k = sorted(cc, key=lambda x: cc[x])[-1]
print k, cc[k]