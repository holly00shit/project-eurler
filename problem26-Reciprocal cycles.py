#!/usr/bin/env python
# -*- coding: utf-8 -*- 

"""
这里还有一个重要的结论
对于一个形如 1 / x的分数
找出最小的999...99使得x能整除999....999 那么 这个999....9有多少位，
这个分数转化为小数循环部分就有多少位。
不过前提是要把2和5因子都除掉。
因为形如999...99是永远没法被5和2整除的。
那么为什么这样就对呢？
我们都知道0.（9）这个循环小数跟1是等价的
所以我们的目标就是让1 /x转化后的小数乘以x然后== 0.(9)
然后实际上就是一堆9凑成的数能被x整除就找到循环节了。
"""

def calculate(x):
    if x%2 == 0 :
        return calculate(x/2)
    if x%5 == 0:
        return calculate(x/5)
    i = 1
    while True:
        if (pow(10, i) - 1) % x == 0: return i
        i = i + 1

m = 0
n = 0
for d in xrange(1, 1000):
    c = calculate(d)
    if c > m:
        m = c
        n = d

print n