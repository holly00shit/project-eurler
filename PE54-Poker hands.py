#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

High Card: Highest value card.
One Pair: Two cards of the same value.
Two Pairs: Two different pairs.
Three of a Kind: Three cards of the same value.
Straight: All cards are consecutive values.
Flush: All cards of the same suit.
Full House: Three of a kind and a pair.
Four of a Kind: Four cards of the same value.
Straight Flush: All cards are consecutive values of same suit.
Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.

If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.

Consider the following five hands dealt to two players:

Hand	 	Player 1	 	Player 2	 	Winner
1	 	5H 5C 6S 7S KD
Pair of Fives
 	2C 3S 8S 8D TD
Pair of Eights
 	Player 2
2	 	5D 8C 9S JS ACH
Highest card Ace
 	2C 5C 7D 8S QH
Highest card Queen
 	Player 1
3	 	2D 9C AS AH AC
Three Aces
 	3D 6D 7D TD QD
Flush with Diamonds
 	Player 2
4	 	4D 6S 9H QH QC
Pair of Queens
Highest card Nine
 	3D 6D 7H QD QS
Pair of Queens
Highest card Seven
 	Player 1
5	 	2H 2D 4C 4D 4S
Full House
With Three Fours
 	3C 3D 3S 9S 9D
Full House
with Three Threes
 	Player 1
The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.

How many hands does Player 1 win?
'''

OrderList = [2, 3, 4, 5, 6, 7, 8, 9, 'T', 'J', 'Q', 'K', 'A']
KeyMap = {'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}

def separateValueAndSuite(hands):
    value_suite = []
    for poker in hands:
        if poker[0] in ['T', 'J', 'Q', 'K', 'A']:
            value_suite.append([KeyMap[poker[0]], poker[1]])
        else:
            value_suite.append([int(poker[0]), poker[1]])
    return value_suite

def sortedcard(value_suites):
    return sorted(value_suites, key=lambda x:x[0])

def highcard(poker1, poker2):
    if OrderList.index(poker1) > OrderList.index(poker2):
        return True
    else:
        return False

def isOfsamesuite(value_suites):
    suites = [vs[1] for vs in value_suites]
    start = suites[0]
    for s in suites:
        if s != start:
            return False
    return True

def isPairs(value_suites):
    values = [vs[0] for vs in value_suites]
    conut = dict.fromkeys(values, 0)
    pairs = []
    threepairs = []
    fourkind = []
    for v in values:
        conut[v] += 1
    for k, v in conut.iteritems():
        if v == 2:
            pairs.append(k)
        if v == 3:
            threepairs.append(k)
        if v == 4:
            fourkind.append(k)
    if len(pairs) == 1:
        if len(threepairs) == 1:
            return 5, pairs,threepairs, fourkind
        else:
            return 1, pairs,threepairs, fourkind
    elif len(threepairs) == 1:
        return 3, pairs,threepairs, fourkind
    elif len(pairs) == 2:
        return 2, pairs,threepairs, fourkind
    elif len(fourkind) == 1:
        return 4, pairs,threepairs, fourkind
    else:
        return False, [], [], []

def isConsecutive(value_suites):
    values = [vs[0] for vs in value_suites]
    for i in range(0, 5):
        k = 0
        while True:
            k = k + 1
            if values[i] + k not in values:
                break
        if k == 5:
            return True, 5, values[i] + 4
        elif values[i] + 1 not in values:
            return False, 0, 0

def cmpList(list1, list2):
    for i in range(len(list1)-1, 0, -1):
        if list1[i] > list2[i]:
            return True
        elif list1[i] == list2[i]:
            continue
        else:
            return False


def analyse(value_suites):
    cflag, clen, cmax = isConsecutive(value_suites)
    plen, ppair,threepairs, pfour = isPairs(value_suites)
    sflag = isOfsamesuite(value_suites)
    values = [vs[0] for vs in value_suites]
    btype = 0
    if cflag and sflag and value_suites[-1][0] == 14:
        btype = 10
    elif cflag and sflag and value_suites[-1][0] != 14:
        btype = 9
    elif plen == 4:
        btype = 8
    elif plen == 5:
        btype = 7
    elif sflag:
        btype = 6
    elif cflag and clen == 5:
        btype = 5
    elif plen == 3:
        btype = 4
    elif plen == 2:
        btype = 3
    elif plen == 1:
        btype = 2
    #return [btype, clen, cmax, plen, pfour, ppair, values]
    return btype, cflag, clen, cmax, plen, ppair,threepairs, pfour, sflag, values

def getWinner(hand1, hand2):
    btype1, cflag1, clen1, cmax1, plen1, ppair1,threepairs1, pfour1, sflag1, values1 = hand1
    btype2, cflag2, clen2, cmax2, plen2, ppair2,threepairs2, pfour2, sflag2, values2 = hand2
    if btype1 > btype2:
        return True
    elif btype1 == btype2:
        if btype1 == 9 and cmpList(values1, values2):
            return True
        elif btype1 == 8:
            if pfour1[0] > pfour2[0]:
                return True
            elif cmpList(values1, values2):
                return True
            else:
                return False
        elif btype1 == 7:
            if threepairs1[0] > threepairs2[0]:
                return True
            elif threepairs1[0] == threepairs2[0]:
                if ppair1[0] > ppair2[0]:
                    return True
                elif cmpList(values1, values2):
                    return True
                else:
                    return False
        elif btype1 == 6 and cmpList(values1, values2):
                return True
        elif btype1 == 5 and cmpList(values1, values2):
            return True
        elif btype1 == 4:
            if cmax1 > cmax2:
                return True
            if cmax1 == cmax2:
                if cmpList(values1, values2):
                    return True
                else:
                    return False
        elif btype1 == 3:
            if cmpList(sorted(ppair1), sorted(ppair2)):
                return True
            elif cmpList(values1, values2):
                return True
            else:
                return False
        elif btype1 == 2:
            if ppair1[0] > ppair2[0]:
                return True
            elif ppair1[0] == ppair2[0]:
                if cmpList(values1, values2):
                    return True
                else:
                    return False
            return False
        elif btype1 == 0:
            if cmpList(values1, values2):
                return True
            else:
                return False
        return False
    return False

with open('poker.txt') as fp:
    count = 0
    for line in fp.readlines():
        hand1 = line.strip().split()[ :5]
        hand2 = line.strip().split()[5: ]
        poker1 = sortedcard(separateValueAndSuite(hand1))
        poker2 = sortedcard(separateValueAndSuite(hand2))
        if getWinner(analyse(poker1), analyse(poker2)):
            count += 1
    print count


learn = """
def poker(hands):
    '''returns the winning hand'''
    hands = collect(hands)
    if hand_rank(hands[0]) > hand_rank(hands[1]):
        return '1'
    else:
        return '2'

def rank_score(hand):
    '''returns an ordered (reverse) list of ranks'''
    ranks = ['--23456789TJQKA'.index(r) for r, s in hand]
    ranks.sort(reverse = True)
    if ranks == [14,5,4,3,2]:
        return [5,4,3,2,1]
    return ranks

def hand_rank(hand):
    '''ranks a hand according to poker rules'''
    ranks = rank_score(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks))
    elif flush(hand):
        return (5, rank_score(hand))
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, rank_score(hand)):
        return (3, kind(3, ranks))
    elif two_pair(ranks):
        return (2, two_pair(rank_score(hand)))
    elif kind(2, ranks):
        return (1, kind(2, ranks), rank_score(hand))
    else:
        return (0, rank_score(hand))

def straight(ranks):
    '''return True if straight; False, otherwise'''
    return max(ranks) - min(ranks) == 4 and len(set(ranks)) == 5

def flush(hand):
    '''returns True if flush; Fasle, otherwise'''
    suits = [s for r, s in hand]
    return len(set(suits)) == 1

def kind(n, ranks):
    '''returns the first instance of a n same cards'''
    for rank in ranks:
        if ranks.count(rank) == n:
            return rank
    return None

def two_pair(ranks):
    '''returns 2 pairs as a tuple with higher value first'''
    first = kind(2, ranks)
    ranks.sort()
    second = kind(2, ranks)
    if first and second != first:
        return (first, second)
    return None

def collect(row):
    '''turns a row in a file into a list of 2 lists (players' hands)'''
    lst = row.split()
    player_1 = lst[:5]
    player_2 = lst[5:]
    result = [player_1, player_2]
    return result

result = 0
f = open('poker.txt', 'r')
x = f .readline()
while x != '':
    if poker(x) == '1':
        result += 1
    x = f.readline()
print result
f.close()
"""

good="""
value = { '2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'T':10,'J':11,'Q':12,'K':13,'A':14 }
all_kinds = tuple(reversed(sorted(value.values())))
all_suits = list('DCSH')

def make_hand(cards):
    hand = {}
    for card in cards:
        hand.setdefault(value[card[0]], {})[card[1]] = 1
        hand.setdefault(card[1], {})[value[card[0]]] = 1
    return hand

def get(hash, arr): return ((i, hash.get(i, {})) for i in arr)
def has(hash, arr): return not sum(1 for i in arr if i not in hash)

def rank(hand):
    # Royal flush
    for suit, kinds in get(hand, all_suits):
        if has(kinds, tuple('TJQKA')):
            return (9,)

    # Straight flush
    for suit, kinds in get(hand, all_suits):
        kinds = sorted(kind for kind in kinds.keys())
        if len(kinds) == 5 and kinds[4] - kinds[0] == 4:
            return (8, kinds[0])

    # Four of a kind
    for kind, suits in get(hand, all_kinds):
        if len(suits.keys()) == 4:
            return (7, kind)

    # Full house
    for kind, suits in get(hand, all_kinds):
        if len(suits.keys()) == 3:
            for kind2, suits2 in get(hand, all_kinds):
                if len(suits2.keys()) == 2:
                    return (6, kind, kind2)

    # Flush
    for suit, kinds in get(hand, all_suits):
        if len(kinds.keys()) == 5:
            return (5,)

    # Straight
    kinds = sorted(kind for kind in all_kinds if hand.has_key(kind))
    if len(kinds) == 5 and kinds[4] - kinds[0] == 4:
        return (4, kinds[0])

    # Three of a kind
    for kind, suits in get(hand, all_kinds):
        if len(suits.keys()) == 3:
            return (3, kind)

    # Two pairs
    for kind, suits in get(hand, all_kinds):
        if len(suits.keys()) == 2:
            for kind2, suits2 in get(hand, all_kinds):
                if kind != kind2 and len(suits2.keys()) == 2:
                    return (2, kind, kind2)

    # One pair
    for kind, suits in get(hand, all_kinds):
        if len(suits.keys()) == 2:
            return (1, kind)

    for kind in all_kinds:
        if kind in hand:
            return (0, kind)

    return 0


count = 0
for hand in open('poker.txt'):
    hands = hand.split(' ')
    p1, p2 = make_hand(hands[0:5]), make_hand(hands[5:10])
    v1, v2 = rank(p1), rank(p2)
    if v1 > v2: count += 1; print hands[0:5], hands[5:10]
print count
"""














