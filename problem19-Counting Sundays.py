#!/usr/bin/env python
# -*- coding: utf-8 -*- 

month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
month2 = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

days = 365
count = 0
for year in range(1901, 2000):
    for mon in range(0, 12):
        if year%400 == 0 or year%4 == 0:
            days += month2[mon]
        else:
            days += month[mon]

        if days%7 == 0:
            count += 1

print count