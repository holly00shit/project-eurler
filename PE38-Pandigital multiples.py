#!/usr/bin/env python
# -*- coding: utf-8 -*- 

'''
将192与1,2,3分别相乘得到：

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
将这三个乘积连接起来我们得到一个1到9的pandigital数， 192384576。我们称 192384576是192和 (1,2,3)的连接积。
通过将9与1,2,3,4和5相乘也可以得到pandigital数：918273645，这个数是9和(1,2,3,4,5)的连接积。
用一个整数与1,2, ... , n（n大于1）的连接积构造而成的1到9pandigital数中最大的是多少？
'''

def isPandigital(num, length = False):
    num_str = str(num)
    pans = []
    for i in num_str:
        i = int(i)
        if i not in range(1, 10):
            return False
        if i not in pans:
            pans.append(i)
        elif i in pans:
            return False
    if length and len(pans) != 9:
        return False
    return True

rel = []
for x in range(1, 9999):
    flag = True
    tt = 0
    for y in range(1, 9):
        tb = x * y
        if not isPandigital(tb):
            flag = False
            break
        else:
            tt = int(str(tt) + str(tb))
            #print x, y, tt
        if isPandigital(tt, length=True):
            rel.append([x, y, tt])
    if not flag:
        continue

#print rel
large = 0
for ele in rel:
    if ele[-1] > large:
        large = ele[-1]

print large


nice_work = '''
import sys

multiples = []

def check(n, k):
    m = ''.join([ str(n * x) for x in xrange(1, k + 1) ])
    if ''.join(sorted(m)) == '123456789':
        multiples.append(m)

def main():
    for n in xrange(2,10000):
        for k in xrange(2, 5):
            check(n, k)
    print max(multiples)

if __name__ == '__main__':
    sys.exit(main())
'''