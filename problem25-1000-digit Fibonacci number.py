# -*- coding: utf-8 -*-


def Fibonacci():
    FBlist = [1,1]
    constInt = 1000
    FBstr = ''
    i = 2
    while len(FBstr) < constInt:
        iFB = FBlist[i-1] + FBlist[i-2]
        FBlist.append(iFB)
        FBstr = str(FBlist[i])
        i += 1
    return i


if __name__ == '__main__':
    print Fibonacci()


