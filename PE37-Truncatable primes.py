#!/usr/bin/env python
# -*- coding: utf-8 -*- 
'''
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

http://projecteuler.net/problem=37
'''


#primes = [True]*10000000
#for i in range(2, 10000000):
#    for k in range(i*2, 10000000, i):
#        primes[k] = False
#primes[1] = False
import math

def isPrime(n):
    '''judge if prime'''
    if n < 2 : return False
    for i in xrange(2, int(math.sqrt(n))+1):
        if n%i == 0:
            return False
    return True


def isTruncatable(num):
    d = str(num)
    for i in range(len(d)):
        if not isPrime(int(d[i:])): return False
    for i in range(len(d), 0, -1):
        if not isPrime(int(d[:i])): return False
    return True

sum = 0
nums = 0
for i in range(10, 10000000):
    if not isPrime(i): continue
    if isTruncatable(i): sum = sum + i; print i; nums += 1
    if nums == 11: break
print sum
