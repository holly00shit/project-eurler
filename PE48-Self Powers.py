#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.
'''


s = 0
mod = pow(10, 10)
for x in xrange(1, 1001):
    s = s + pow(x, x)

print s % mod