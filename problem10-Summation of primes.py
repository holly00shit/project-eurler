#coding:utf8
import math
import time

__author__ = 'Administrator'
""" decathlete """

# my first method  too slow!!!!
def isPrimeFlag(n):
    if n == 2:
        return True
    for i in range(2,int(math.sqrt(n))+1):
        if n%i == 0 :
            return False
    return True

def sum():
    startTime = time.time()
    p = 0
    for i in range(2,2000000):
        if isPrimeFlag(i):
            p += i
    print ("Elapsed Time = %f seconds" %(time.time() - startTime))
    print p


#prim sieve !!!!

Limit = 2000000

#Create arry and assume all numbers are prime
arr = [True] * Limit

def PrimeSieve(x):
    global arr,limit
    for i in range(x*2,Limit,x):
        arr[i] = False

map(PrimeSieve,range(2,Limit**1/2))  # Only go up to square root of the end

def SieveSum():
    startTime = time.time()
    p = 0
    for i in range(2,Limit):
        if arr[i]: p += i
    print ("SIEVE : Elapsed Time = %f seconds" %(time.time() - startTime))
    print p



if __name__ == '__main__':
#    sum()
    SieveSum()





