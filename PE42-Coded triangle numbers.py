#!/usr/bin/env python
# -*- coding: utf-8 -*- 

letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def isTriangleNum(num):
    for i in range(1, num**1/2 + 2): #notice num=1
        if i*(i+1)/2 == num:
            return True
    return False

rel = []
fp = open('words.txt', 'r')
for word in fp.read().split(','):
    word = word.strip().strip('"')
    sum = 0
    for letter in word:
        sum = sum + letters.index(letter) + 1
    if isTriangleNum(sum):
        rel.append(word)

print rel.__len__()

