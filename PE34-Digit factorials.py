#!/usr/bin/env python
# -*- coding: utf-8 -*- 


def factorial(n):
    if n == 1:
        return n
    return n*factorial(n-1)

fact = [1] + [factorial(i) for i in range(1, 10)]

def sum_of_digits_factorial(n):
    s = 0
    while n > 0:
        d = n % 10
        s = s + fact[d]
        n = n / 10
    return s

print sum(n for n in xrange(10, 100000) if n == sum_of_digits_factorial(n))