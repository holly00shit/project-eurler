#!/usr/bin/env python
# -*- coding: utf-8 -*-



'''
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×12
15 = 7 + 2×22
21 = 3 + 2×32
25 = 7 + 2×32
27 = 19 + 2×22
33 = 31 + 2×12

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
'''

import math


MAX = 10000
prime = [True]*MAX
def prime_sieve(max):
    global  prime
    for i in range(2, max**1/2 + 1):
        for n in range(i+i, MAX, i):
            prime[n] = False

prime_sieve(MAX)
prime[0] = False
prime[1] = False

Flag = True
start = 5

while Flag:
    Flag = False
    if prime[start]:
        Flag = True
        start = start + 2
        continue
    for i in range(2, start):
        if prime[i]:
            solve = math.sqrt((start - i)/2)
            if solve != 0 and  solve - int(solve) == 0:
                Flag = True
                break
    if Flag:   start = start + 2

print start